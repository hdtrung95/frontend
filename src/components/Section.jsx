import React from 'react'

function Section() {
    return (
        <div>
            <div className="row row-cols-1 row-cols-md-3">
                <div className="col mb-4">
                    <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Machine</h5>
                        <p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>

                    </div>
                    <div className="btn-group" role="group" aria-label="Basic example">
                        <a href='#' className="btn btn-secondary">Detail</a>
                        <a href='#' className="btn btn-secondary">Edit</a>
                        <a href='#' className="btn btn-secondary">Delete</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Section;
